package br.edu.up.app;

import br.edu.up.exercicios.*;
import java.util.Scanner;

public class Menu {

	public static void mostrar() {

		// Será utilizado apenas um Scanner
		// em todos os exercícios.
		Scanner leitor = new Scanner(System.in);

		int opcao = 0;

		do {
			System.out.println("Digite uma das opções:");
			int qtdeDeExercicios = 15;
			for (int i = 1; i <= qtdeDeExercicios; i++) {
				System.out.println("> Exercício (" + i + ")");
			}
			System.out.println("> Sair (0)");
			System.out.println();
			
			opcao = leitor.nextInt();
			
			//Avança o leitor para próxima linha.
			leitor.nextLine();

			System.out.println();

			switch (opcao) {
			case 1:
				ListaDeExercicios.rodarExe01(leitor);
				break;
			case 2:
				ListaDeExercicios.rodarExe02(leitor);
				break;
			case 3:
				ListaDeExercicios.rodarExe03(leitor);
				break;
			case 4:
				ListaDeExercicios.rodarExe04(leitor);
				break;
			case 5:
				ListaDeExercicios.rodarExe05(leitor);
				break;
			case 6:
				ListaDeExercicios.rodarExe06(leitor);
				break;
			case 7:
				ListaDeExercicios.rodarExe07(leitor);
				break;
			case 8:
				ListaDeExercicios.rodarExe08(leitor);
				break;
			case 9:
				ListaDeExercicios.rodarExe09(leitor);
				break;
			case 10:
				ListaDeExercicios.rodarExe10(leitor);
				break;
			case 11:
				ListaDeExercicios.rodarExe11(leitor);
				break;
			case 12:
				ListaDeExercicios.rodarExe12(leitor);
				break;
			case 13:
				ListaDeExercicios.rodarExe13(leitor);
				break;
			case 14:
				ListaDeExercicios.rodarExe14(leitor);
				break;
			case 15:
				ListaDeExercicios.rodarExe15(leitor);
				break;
			}
			System.out.println();
		} while (opcao != 0);

		leitor.close();
		System.out.println("Programa encerrado!");
	}
}