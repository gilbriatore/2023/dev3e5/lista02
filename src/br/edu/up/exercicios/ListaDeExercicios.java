package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class ListaDeExercicios {

	public static void rodarExe15(Scanner leitor) {

		String s = "Ex 15. Elabore um algoritmo que leia 5 números do teclado e preencha um vetor\r\n"
				+ "de acordo com a seguinte regra: com exceção do 1º número, só é permitido\r\n"
				+ "armazenar um número se ele for maior que o anterior. Ex.: se o primeiro valor\r\n"
				+ "lido for 5, o próximo valor lido só poderá ser maior que 5.";

		System.out.println(s + "\n");

		int vetor[] = new int[5];

		int contador = 0;
		while (contador < 5) {
			System.out.println("Digite o elemento " + (contador + 1) + " do vetor:");
			int valor = leitor.nextInt();
			if (contador == 0) {
				vetor[contador] = valor;
				contador = contador + 1;
			} else {
				if (valor > vetor[contador - 1]) {
					vetor[contador] = valor;
					contador = contador + 1;
				}
			}
		}
		System.out.println();
		System.out.println("O vetor obtido é:\n");

		contador = 0;
		while (contador < 5) {
			if (contador > 0) {
				System.out.print(", ");
			}
			System.out.print(vetor[contador]);
			contador = contador + 1;
		}

		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}

	public static void rodarExe14(Scanner leitor) {

		String s = "Ex 14. Faça um programa que leia um vetor de 5 elementos e, após a leitura,\r\n"
				+ "posicione o maior elemento na última posição do vetor. Nenhum número do vetor\r\n"
				+ "pode ser apagado ou duplicado. Apresente o vetor atualizado na tela.";

		System.out.println(s + "\n");

		int qtde = 5, vlrmaior = 0, posmaior = 4;
		int[] vetor = new int[qtde];

		int contador = 0;
		while (contador < qtde) {
			System.out.println("Digite o elemento " + (contador + 1) + " do vetor:");
			vetor[contador] = leitor.nextInt();
			contador = contador + 1;
		}
		System.out.println("------------------\n");

		contador = 0;
		while (contador < qtde) {
			if (vetor[contador] > vlrmaior) {
				vlrmaior = vetor[contador];
				posmaior = contador;
			}
			contador = contador + 1;
		}

		vetor[posmaior] = vetor[qtde - 1];
		vetor[qtde - 1] = vlrmaior;

		System.out.println("O vetor atualizado é:\n");

		contador = 0;
		while (contador < qtde) {
			if (contador > 0) {
				System.out.print(", ");
			}
			System.out.print(vetor[contador]);
			contador = contador + 1;
		}

		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}

	public static void rodarExe13(Scanner leitor) {

		String s = "Ex 13. Elabore um algoritmo que receba um número n e retorne um vetor com os\r\n"
				+ "n primeiros termos da sequência de Fibonacci. Exemplo: n = 8, vetor = {1, 1,\r\n"
				+ "2, 3, 5, 8, 13, 21}.";

		System.out.println(s + "\n");

		int n = 1;

		System.out.println("Informe o número n:\n");
		n = leitor.nextInt();

		int[] vetor = new int[n];
		vetor[0] = 1;
		vetor[1] = 1;

		int contador = 2;
		while (contador < n) {
			vetor[contador] = vetor[contador - 2] + vetor[contador - 1];
			contador = contador + 1;
		}
		System.out.println("A sequência de Fibonacci obtida é:\n");

		contador = 0;
		while (contador < n) {
			if (contador > 0) {
				System.out.print(", ");
			}
			System.out.print(vetor[contador]);
			contador = contador + 1;
		}
		
		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}

	public static void rodarExe12(Scanner leitor) {

		String s = "Ex 12. Elabore um programa que leia um vetor de 12 elementos apresente na\r\n"
				+ "tela o produto dos elementos pares positivos (desconsiderar o zero). Exemplo:\r\n"
				+ "{0, 5, 8, 1, -6, 4, -7, 9, 10, -14, 3, 12} = 8 * 4 * 10 * 12 = 3840";

		System.out.println(s + "\n");

		int qtde = 12;
		int produto = 1;
		int[] vetor = { 0, 5, 8, 1, -6, 4, -7, 9, 10, -14, 3, 12 };

		int contador = 0;
		while (contador < qtde) {
			if (vetor[contador] > 0 && vetor[contador] % 2 == 0) {
				produto *= vetor[contador];
			}
			contador++;
		}

		System.out.println("O produto é: " + produto);

		System.out.println();
		System.out.println("Pressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}

	public static void rodarExe11(Scanner leitor) {

		String s = "Ex 11. Elabore um algoritmo que calcule o produto escalar entre dois vetores\r\n"
				+ "de inteiros de tamanho igual a 5. Exemplo: {0, 2, 4, 6, 8}, {1, 3, 5, 7, 9} =\r\n"
				+ "0*1 + 2*3 + 4*5 + 6*7 + 8*9 = 140";

		System.out.println(s + "\n");

		int qtde = 5;
		int produto = 0;
		int[] vetor1 = { 0, 2, 4, 6, 8 };
		int[] vetor2 = { 1, 3, 5, 7, 9 };

		int contador = 0;
		while (contador < qtde) {
			produto += vetor1[contador] * vetor2[contador];
			contador++;
		}

		System.out.println("O produto escalar é: " + produto);

		System.out.println();
		System.out.println("Pressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}

	public static void rodarExe10(Scanner leitor) {

		String s = "Ex 10. Crie um programa que leia um vetor A de 5 posições e, ao final da\r\n"
				+ "leitura, copie os elementos de A em B de forma invertida. Ou seja, o primeiro\r\n"
				+ "elemento de A é o último elemento de B, o segundo elemento de A é o penúltimo\r\n"
				+ "elemento de B, e assim por diante.";

		System.out.println(s + "\n");

		int qtde = 3;
		int[] a = new int[qtde];
		int[] b = new int[qtde];

		int contador = 0;
		while (contador < qtde) {
			System.out.print("Digite o elemento " + (contador + 1) + " do vetor A: ");
			a[contador] = leitor.nextInt();
			contador++;
		}
		System.out.println("------------------");

		contador = 0;
		int contadorInvertido = qtde - 1;
		while (contador < qtde) {
			b[contador] = a[contadorInvertido];
			contadorInvertido--;
			contador++;
		}

		System.out.println("Elementos do vetor B:");
		contador = 0;
		while (contador < qtde) {
			if (contador > 0) {
				System.out.print(", ");
			}
			System.out.print(b[contador]);
			contador++;
		}

		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}

	public static void rodarExe09(Scanner leitor) {

		String s = "Ex 9. Elabore um programa que leia os vetores A e B de 5 elementos e gere um\r\n"
				+ "vetor C de acordo com a seguinte regra: a. Os elementos das posições pares de\r\n"
				+ "C são os elementos das posições pares de A; b. Os elementos das posições\r\n"
				+ "ímpares de C são os elementos das posições ímpares de B.";

		System.out.println(s + "\n");

		int qtde = 5;
		int[] a = new int[qtde];
		int[] b = new int[qtde];
		int[] c = new int[qtde];

		int contador = 0;
		while (contador < qtde) {
			System.out.println("Digite o elemento " + (contador + 1) + " do vetor A:");
			a[contador] = leitor.nextInt();
			contador = contador + 1;
		}
		System.out.println("------------------\n");

		contador = 0;
		while (contador < qtde) {
			System.out.println("Digite o elemento " + (contador + 1) + " do vetor B:");
			b[contador] = leitor.nextInt();
			contador = contador + 1;
		}
		System.out.println("------------------\n");

		contador = 0;
		while (contador < qtde) {
			if (contador % 2 == 0) {
				c[contador] = b[contador];
			} else {
				c[contador] = a[contador];
			}
			contador = contador + 1;
		}

		System.out.println("Elementos do vetor C:\n");
		contador = 0;
		while (contador < qtde) {
			if (contador > 0) {
				System.out.print(", ");
			}
			System.out.print(c[contador]);
			contador = contador + 1;
		}

		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}

	public static void rodarExe08(Scanner leitor) {

		String s = "Ex 8. Dados dois vetores A e B de 5 elementos cada, criar um vetor C que\r\n"
				+ "representa a concatenação de A e B. Ou seja, C contém os elementos de A\r\n"
				+ "seguidos dos elementos de B.";

		System.out.println(s + "\n");

		int qtde = 5;
		int a[] = { 10, 9, 3, 5, 7 };
		int b[] = { 4, 5, 7, 9, 1 };
		int c[] = new int[qtde * 2];

		int contador = 0;
		while (contador < qtde) {
			c[contador] = a[contador];
			c[(contador + qtde)] = b[contador];
			contador = contador + 1;
		}
		System.out.println("------------------\n");

		System.out.println("Valores do vetor C:\n");
		contador = 0;
		while (contador < (qtde * 2)) {
			if (contador > 0) {
				System.out.print(", ");
			}
			System.out.print(c[contador]);
			contador = contador + 1;
		}

		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}

	public static void rodarExe07(Scanner leitor) {

		String s = "Ex 7. Faça um programa que leia dois vetores de 5 elementos cada e verifique\r\n"
				+ "se eles são iguais ou não. Para serem iguais, todos os elementos dos dois\r\n"
				+ "vetores devem coincidir.";

		System.out.println(s + "\n");

		int qtde = 3;
		double[] vetor1 = new double[qtde];
		double[] vetor2 = new double[qtde];
		boolean iguais = true;

		int contador = 0;
		while (contador < qtde) {
			System.out.println("Digite o elemento " + (contador + 1) + " do vetor 1:");
			vetor1[contador] = leitor.nextDouble();
			contador = contador + 1;
		}
		System.out.println("------------------\n");

		contador = 0;
		while (contador < qtde) {
			System.out.println("Digite o elemento " + (contador + 1) + " do vetor 2:");
			vetor2[contador] = leitor.nextDouble();
			contador = contador + 1;
		}
		System.out.println("------------------\n");

		contador = 0;
		while (contador < qtde) {
			if (vetor1[contador] != vetor2[contador]) {
				iguais = false;
			}
			contador = contador + 1;
		}

		if (iguais) {
			System.out.println("Os dois vetores são iguais!");
		} else {
			System.out.println("Os dois vetores são diferentes!");
		}

		System.out.println();
		System.out.println("Pressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}

	public static void rodarExe06(Scanner leitor) {

		String s = "Ex 6. Elabore um programa que leia um vetor com 5 notas de um aluno e um\r\n"
				+ "outro vetor com 5 pesos e calcule a média ponderada do aluno. Ex.: notas:\r\n"
				+ "7.5, 9.2 pesos: 6, 4 média ponderada: (7.5 * 6 + 9.2 * 4) / (6 + 4)\r\n" + "\r\n"
				+ "Cálculo da média ponderada: (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) /\r\n"
				+ "(peso1 + peso2 + peso3)";

		System.out.println(s + "\n");

		int qtde = 5;
		double[] notas = new double[qtde];
		double[] pesos = new double[qtde];

		int contador = 0;
		while (contador < qtde) {
			System.out.println("Digite a nota " + (contador + 1) + ":");
			notas[contador] = leitor.nextDouble();
			contador = contador + 1;
		}

		contador = 0;
		while (contador < qtde) {
			System.out.println("Digite o peso " + (contador + 1) + ":");
			pesos[contador] = leitor.nextDouble();
			contador = contador + 1;
		}

		contador = 0;
		double dividendo = 0, divisor = 0;
		while (contador < qtde) {
			dividendo = dividendo + (notas[contador] * pesos[contador]);
			divisor = divisor + pesos[contador];
			contador = contador + 1;
		}

		System.out.println("A média ponderada é: " + (dividendo / divisor));

		System.out.println();
		System.out.println("Pressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
		;
	}

	public static void rodarExe05(Scanner leitor) {

		String s = "Ex 5. Elabore um programa que leia 5 números do teclado e os armazene em um\r\n"
				+ "vetor. Em seguida, leia um número n qualquer e apresente na tela a quantidade\r\n"
				+ "de vezes que o número n aparece no vetor.";

		System.out.println(s + "\n");

		int qtde = 5, n, qtdex = 0;
		int[] vetor = new int[qtde];

		int contador = 0;
		while (contador < qtde) {
			System.out.println("Digite o número " + (contador + 1) + ":");
			vetor[contador] = leitor.nextInt();
			contador = contador + 1;
		}

		System.out.println("Informe o número n:");
		n = leitor.nextInt();

		contador = 0;
		while (contador < qtde) {
			if (vetor[contador] == n) {
				qtdex = qtdex + 1;
			}
			contador = contador + 1;
		}

		System.out.println("O número aparece " + qtdex + " vezes!");

		System.out.println();
		System.out.println("Pressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}

	public static void rodarExe04(Scanner leitor) {

		String s = "Ex 4. Elabore um programa que leia um vetor de 5 elementos e um número n\r\n"
				+ "qualquer. Mostre na tela o índice dos elementos que são inferiores a n.";

		System.out.println(s + "\n");

		int qtde = 5, n;
		int[] vetor = new int[qtde];

		int contador = 0;
		while (contador < qtde) {
			System.out.println("Digite o número " + (contador + 1) + ":");
			vetor[contador] = leitor.nextInt();
			contador = contador + 1;
		}

		System.out.println("Informe o número n:\n");
		n = leitor.nextInt();

		contador = 0;
		while (contador < qtde) {
			if (vetor[contador] < n) {
				System.out.println(
						"O número " + vetor[contador] + " é menor do que " + n + " e seu índice é: " + contador);
			}
			contador = contador + 1;
		}

		System.out.println();
		System.out.println("Pressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}

	public static void rodarExe03(Scanner leitor) {
		String s = "Ex 3. Faça um programa que leia 5 números do teclado e os armazene em um\r\n"
				+ "vetor. Crie um segundo vetor que armazene o dobro de cada um dos números do\r\n"
				+ "primeiro vetor e apresente este vetor.";

		System.out.println(s + "\n");

		int qtde = 5;
		String[] vetor01 = new String[qtde];
		String[] vetor02 = new String[qtde];

		for (int i = 0; i < vetor01.length; i++) {
			System.out.println("Digite um número:");
			// int numero = leitor.nextInt();
			vetor01[i] = leitor.nextLine();
		}

		for (int i = 0; i < vetor01.length; i++) {
			String vlrVetor01 = vetor01[i];
			// int dobro = Integer.parseInt(vlrVetor01) * 2;
			Integer dobro = Integer.parseInt(vlrVetor01) * 2;
			vetor02[i] = dobro.toString();
		}

		for (int i = 0; i < vetor02.length; i++) {
			String valor = vetor02[i];
			System.out.println("Valor em dobro: " + valor);
		}

		System.out.println();
		System.out.println("Pressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}

	public static void rodarExe02(Scanner leitor) {
		String s = "Ex 2. Construa um programa que leia um vetor de 5 elementos e calcule a média\r\n"
				+ "destes valores. Na seqüência, apresente na tela os valores que são iguais ou\r\n"
				+ "superiores à média.";

		System.out.println(s + "\n");

		int qtde = 5;
		int[] vetor = new int[qtde];
		int total = 0;

		for (int i = 0; i < vetor.length; i++) {
			System.out.println("Informe o elemento " + (i + 1) + ":");
			vetor[i] = leitor.nextInt();
			// total = total + vetor[i];
			total += vetor[i];
		}

		// int media = (vetor[0] + vetor[1] + vetor[2] + vetor[3] + vetor[4]) / 5;
		// int total = (vetor[0] + vetor[1] + vetor[2] + vetor[3] + vetor[4]);
		int media = total / qtde;

		System.out.println();
		System.out.println("--------RELATÓRIO----------");
		System.out.println();
		System.out.println("Média: " + media);
		System.out.println();

		for (int i = 0; i < vetor.length; i++) {
			int numero = vetor[i];
			if (numero == media) {
				System.out.println("O número " + numero + " é igual a média!");
			} else if (numero > media) {
				System.out.println("O número " + numero + " é superior a média!");
			} else {
				System.out.println("O número " + numero + " é inferior a média!");
			}
		}

		System.out.println();
		System.out.println("Pressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}

	public static void rodarExe01(Scanner leitor) {

		String s = "Ex 1. Escreva um algoritmo que leia uma sequência finita de números e mostre\r\n"
				+ "positivo, negativo ou zero para cada número.";

		System.out.println(s + "\n");

		System.out.println("Informe a quantidade de números:");
		int qtde = leitor.nextInt();
		// int[] vetor = {8, 5, -3};
		int[] vetor = new int[qtde];

		// repetidor para preencher o vetor
		int contador = 0;
		while (contador < qtde) {
			System.out.println("Informe o número " + (contador + 1) + " de " + qtde + ":");
			vetor[contador] = leitor.nextInt();
			contador++;
		}

		// repetidor para mostrar o tipo de número
		for (int i = 0; i < qtde; i++) {

			int numero = vetor[i];
			if (numero < 0) {
				System.out.println("Negativo!");
			} else if (numero == 0) {
				System.out.println("Zero!");
			} else {
				System.out.println("Positivo!");
			}

		}

		System.out.println();
		System.out.println("Pressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}

}
